package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.databinding.ActivityResulatactivityBinding
import com.example.myapplication.ui.main.ResulatactivityFragment

class Resulatactivity : AppCompatActivity() {

    private lateinit var binding: ActivityResulatactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_resulatactivity)
        binding= ActivityResulatactivityBinding.inflate(layoutInflater)
        val view =binding.root
        setContentView(view)
        val score=intent.getIntExtra("right_answer_count",0)
        binding.resultlabel.text=getString(R.string.resulat_score,score)
binding.tryAgainBtn.setOnClickListener {
    startActivity(Intent(this@Resulatactivity,MainActivity::class.java))
}
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ResulatactivityFragment.newInstance())
                .commitNow()
        }
    }
}