package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private  var rightAnswer:String?=null
    private var rightAnswerCount=0
    private var quizCount=1
    private val QizCount=10

    private val quizdata = mutableListOf(
        // country, capital, choice1, choice2, choice3
        mutableListOf("China", "Beijing", "Jakarta", "Manila", "Stockholm"),
        mutableListOf("India", "New Delhi", "Beijing", "Bangkok", "Seoul"),
        mutableListOf("Indonesia", "Jakarta", "Manila", "New Delhi", "Kuala Lumpur"),
        mutableListOf("Japan", "Tokyo", "Bangkok", "Taipei", "Jakarta"),
        mutableListOf("Thailand", "Bangkok", "Berlin", "Havana", "Kingston"),
        mutableListOf("Brazil", "Brasilia", "Havana", "Bangkok", "Copenhagen"),
        mutableListOf("Canada", "Ottawa", "Bern", "Copenhagen", "Jakarta"),
        mutableListOf("Cuba", "Havana", "Bern", "London", "Mexico City"),
        mutableListOf("Mexico", "Mexico City", "Ottawa", "Berlin", "Santiago"),
        mutableListOf("United States", "Washington D.C.", "San Jose", "Buenos Aires", "Kuala Lumpur"),
        mutableListOf("France", "Paris", "Ottawa", "Copenhagen", "Tokyo"),
        mutableListOf("Germany", "Berlin", "Copenhagen", "Bangkok", "Santiago"),
        mutableListOf("Italy", "Rome", "London", "Paris", "Athens"),
        mutableListOf("Spain", "Madrid", "Mexico City", "Jakarta", "Havana"),
        mutableListOf("United Kingdom", "London", "Rome", "Paris", "Singapore")


    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        binding=ActivityMainBinding.inflate(layoutInflater)
        val view=binding.root
        setContentView( view)



        quizdata.shuffle()
        showNextQuiz()
    }


    fun showNextQuiz(){
        binding.countlabel.text=getString(R.string.count_label,quizCount)
        val quiz = quizdata[0]
        binding.questionlabel.text= quiz[0]
        rightAnswer=quiz[1]
        quiz.removeAt(0)
        quiz.shuffle()
        binding.answerBtn1.text=quiz[0]
        binding.answerBtn2.text=quiz[1]
        binding.answerBtn3.text=quiz[2]
        binding.answerBtn4.text=quiz[3]

        quizdata.removeAt(0)
    }
    fun checkAnswer(view: View){
val answerBtn:Button=findViewById(view.id)
        val  btnText=answerBtn.text.toString()
        val alerttitle:String
        if (btnText==rightAnswer) {
            alerttitle = "correct answer bravoo!"
            rightAnswerCount++
        }
        else {
            alerttitle ="wrong answer tryy next"
        }
        AlertDialog.Builder(this)
            .setTitle(alerttitle)
            .setMessage("Answer:$rightAnswer")
            .setPositiveButton("ok"){ dialogInterface, i ->
                checkQuizcount()
            }
            .setCancelable(false)
            .show()
    }
    fun checkQuizcount(){
if (quizCount==QizCount){
val intent=Intent(this@MainActivity,Resulatactivity::class.java)
    intent.putExtra("right_Answer_count",rightAnswerCount)
    startActivity(intent)
} else{
    quizCount++
    showNextQuiz()
}
    }




}